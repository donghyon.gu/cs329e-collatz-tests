#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

from timeit import timeit
# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "5 20\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  5)
        self.assertEqual(j, 20)
        
    def test_read_3(self):
        s = "30 30\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 30)
        self.assertEqual(j, 30)

    def test_read_4(self):
        s = "40 50\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 40)
        self.assertEqual(j, 50)
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_4(self):
        v = collatz_eval(1, 9999)
        self.assertEqual(v, 262)

    def test_eval_4(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_4(self):
        v = collatz_eval(5, 5)
        self.assertEqual(v, 6)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1, 40, 112)
        self.assertEqual(w.getvalue(), "1 40 112\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 5, 20, 21)
        self.assertEqual(w.getvalue(), "5 20 21\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 1, 9999, 262)
        self.assertEqual(w.getvalue(), "1 9999 262\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    def test_solve_2(self):
        r = StringIO("1 20\n100 300\n301 310\n950 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 20 21\n100 300 128\n301 310 87\n950 1000 143\n")

    def test_solve_3(self):
        r = StringIO("1 15\n400 500\n600 750\n1000 1500\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 15 20\n400 500 142\n600 750 171\n1000 1500 182\n")
    
    def test_solve_4(self):
        r = StringIO("1 1\n1 9999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n1 9999 262\n")
    
    def test_time (self) :
        t = timeit(collatz_eval(100, 1000), 'from main import collatz_eval', number = 1000)
        print("{:.2f} milliseconds".format(t * 1000))
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
